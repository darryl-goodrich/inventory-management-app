## Laravel REST API with Sanctum & VueJs Frontend
This is a example REST API using auth tokens with Laravel Sanctum for a Inventroy management system.
## Setup
This assumes you have composer and docker installed.

``` Bash
- git clone https://darryl-goodrich@bitbucket.org/darryl-goodrich/inventory-management-app.git
    
- composer install
    
- Copy ".env.example" to ".env"

- Run ./vendor/bin/sail up -d
    
- Run ./vendor/bin/sail artisan migrate

- npm install
    
- npm run watch
```

## Tests
```
- Run ./vendor/bin/sail test
```
