<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
 use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Register new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password'])
        ]);

        $token = $user->createToken('auth-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    /**
     * User login.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required']
        ]);

        if (Auth::attempt($request->only('email', 'password'))) {
            $token = Auth()->user()->createToken('myapptoken')->plainTextToken;

            $response = [
                    'user' => Auth::user(),
                    'token' => $token
                ];

            return response()->json($response, 200);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * User logout.
     *
     * @param  Request  $request
     * @return Array
     */
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Logged out'
        ];
    }
}
