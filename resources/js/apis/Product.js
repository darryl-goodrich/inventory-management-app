import Api from "./Api";
import Csrf from "./Csrf";

export default {
    async getAll() {
        return Api.get("/products");
    },

    async get(id) {
        return Api.get("/products/" + id);
    },

    async create(form) {
        await Csrf.getCookie();

        return Api.post("/products", form);
    },

    async update(id, form) {
        await Csrf.getCookie();

        return Api.put("/products/" + id, form);
    },

    async delete(id) {
        await Csrf.getCookie();

        return Api.delete("/products/" + id);
    },
};
