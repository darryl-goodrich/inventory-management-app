<?php

namespace Tests\Feature\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EditProdcutTest extends TestCase
{
    use DataBaseMigrations;

    public function testEditProductSuccessfully()
    {
        $this->signIn();

        $product = \App\Models\Product::factory()->create();

        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20',
        ];

        $this->json('put', '/api/products/' . $product->id, $payload)
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Iphone 12',
                'description' => 'This is the new Iphone 12',
                'price' => '899.99',
                'quantity' => '20',
            ]);
    }

    public function testOnlyLoggedInUsersCanEditProducts()
    {
        $product = \App\Models\Product::factory()->create();

        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20',
        ];

        $this->json('put', '/api/products/' . $product->id, $payload)
            ->assertStatus(401)
            ->assertJsonFragment([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testRequireFieldsForEditingProduct()
    {
        $this->signIn();

        $payload = [
            'name' => null,
            'description' => null,
            'price' => null,
            'quantity' => null,
        ];

        $this->json('post', '/api/products', $payload)
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'price' => ['The price field is required.'],
                'quantity' => ['The quantity field is required.'],
            ]);
    }
}
