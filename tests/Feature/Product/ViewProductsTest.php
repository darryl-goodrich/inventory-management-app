<?php

namespace Tests\Feature\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ViewProductsTest extends TestCase
{
    use DataBaseMigrations;

    public function testEditProductSuccessfully()
    {
        \App\Models\Product::factory(5)->create();

        $this->json('get', '/api/products')
            ->assertStatus(200)
            ->assertJsonCount(5);
    }
}
