<?php

namespace Tests\Feature\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DeleteProdcutTest extends TestCase
{
    use DataBaseMigrations;

    public function testDeleteProductSuccessfully()
    {
        $this->signIn();

        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20',
        ];

        $product = \App\Models\Product::factory()->create($payload);

        $this->json('delete', '/api/products/' . $product->id)
            ->assertStatus(200);

        $this->assertDatabaseMissing('products', $payload);
    }

    public function testOnlyLoggedInUsersCanEditProducts()
    {
        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20'
        ];

        $product = \App\Models\Product::factory()->create($payload);

        $this->json('delete', '/api/products/' . $product->id)
            ->assertStatus(401);

        $this->assertDatabaseHas('products', $payload);
    }
}
