<?php

namespace Tests\Feature\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateProductTest extends TestCase
{
    use DataBaseMigrations;

    public function testsCreateProductSuccessfully()
    {
        $this->signIn();

        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20',
        ];

        $this->json('post', '/api/products', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'name',
                'description',
                'price',
                'quantity',
            ]);;
    }

    public function testsOnlyLoggedInUsersCanCreateProducts()
    {
        $payload = [
            'name' => 'Iphone 12',
            'description' => 'This is the new Iphone 12',
            'price' => '899.99',
            'quantity' => '20'
        ];

        $this->json('post', '/api/products', $payload)
            ->assertStatus(401)
            ->assertJsonFragment([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testsRequireFieldsForCreatingProduct()
    {
        $this->signIn();

        $payload = [
            'name' => null,
            'description' => null,
            'price' => null,
            'quantity' => null,
        ];

        $this->json('post', '/api/products', $payload)
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'price' => ['The price field is required.'],
                'quantity' => ['The quantity field is required.'],
            ]);
    }
}
