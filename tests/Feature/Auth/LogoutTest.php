<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LogoutTest extends TestCase
{
    use DataBaseMigrations;

    public function testUserIsLoggedOutProperly()
    {
        $this->signIn();
        
        $this->json('get', '/api/user')->assertStatus(200);
        $this->json('post', '/api/logout')
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => 'Logged out',
        ]);
    }
}
